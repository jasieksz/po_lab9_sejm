package jsz.op.st;

import org.json.JSONException;

import java.io.IOException;

/**
 * class reads arguments and initiates methods
 */
public class InputParser {

    public static void run(String[] args) throws IOException, JSONException, IndexOutOfBoundsException {

        String name;
        if(args[0].equals("sumexpenses")) {
            name = args[1];
            Integer id = SejmTracker.parliament.get(name);
            MP mp = new MP(id,name,JsonParser.readJsonFromUrl(makeUrl(name,"expenses")));
            System.out.println(mp.toString()+" wszystkie wydatki "+mp.sumExpenses());
        }
        else if(args[0].equals("smallexpenses")) {
            name = args[1];
            Integer id = SejmTracker.parliament.get(name);
            MP mp = new MP(id,name,JsonParser.readJsonFromUrl(makeUrl(name,"expenses")));
            System.out.println(mp.toString()+" wszystkie wydatki "+mp.smallExpenses());
        }
        else {
            name = args[1]; // KADENCJA
            Parliament.makeParliament(JsonParser.readJsonFromUrl(makeUrl(name, "parliament")));

            if (args[0].equals("avgexpenses"))
                System.out.println("Srednie wydatki " + Parliament.averageExpenses());

            if (args[0].equals("mosttravels"))
                System.out.println("Najwiecej podrozy " + Parliament.mostTravels().toString());

            if (args[0].equals("longesttravels"))
                System.out.println("Najdluza podroz " + Parliament.longestTravels().toString());

            if (args[0].equals("expensivetravels"))
                System.out.println("Najdrozsza podroz " + Parliament.expensiveTravels().toString());

            if (args[0].equals("italytravels"))
                System.out.println("Wloskie podroze " + Parliament.italyTravels().toString());
        }

    }


    private static String makeUrl(String name, String option){

        String url = "https://api-v3.mojepanstwo.pl/dane/poslowie";
        Integer id = SejmTracker.parliament.get(name);
        if (option.equals("expenses"))
            url.join("","/"+id.toString(),".json","?layers[]=wydatki");
        if (option.equals("travels"))
            url.join("","/"+id.toString(),".json","?layers[]=wyjazdy");
        if (option.equals("everything"))
            url.join("","/"+id.toString(),".json","?layers[]=wydatki&layers[]=wyjazdy");
        if (option.equals("parliament"))
            url.join("",".json?conditions[poslowie.kadencja]="+name);

        return url;
    }
}
