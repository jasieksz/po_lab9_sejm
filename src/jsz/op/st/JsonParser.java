package jsz.op.st;

/**
 * class parse JSON file into object map
 */

import org.json.JSONException;
import org.json.JSONObject;


import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;

public class JsonParser {


    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {

        try(InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);
        }
    }

    //TODO : " brzydko - do poprawienia readALL ---- APACHE COMMONS IO ---> gradle!!!
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    //towrzy hash mape klucz,wartosc pliku JSON
    public static HashMap<String,Integer> parseJson(JSONObject jsonObj){ // Czy to będzie <String,String>
        HashMap<String,Integer> result = new HashMap<>();

        return result;

    }



}
