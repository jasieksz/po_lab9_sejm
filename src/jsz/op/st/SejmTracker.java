package jsz.op.st;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SejmTracker {

    public static Map<String,Integer> parliament = new HashMap<>(); // NAME -> ID posłowie wszystkich kadnecji
    private final static String allMPsUrl = "https://api-v3.mojepanstwo.pl/dane/poslowie.json";

    public static void main(String[] args) {
        try {
            System.out.println(run(args));
        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    //TODO : "Catching exceptions"
    private static String run(String[] args) {

        try {
            //TODO : "potrzebuje stworzyc na samym poczatku mape poslow, ale nie wiem dla ktorej kadencji, robie to dopiero w InputParser"
            parliament = Parliament.makeParliament(JsonParser.readJsonFromUrl(allMPsUrl));
            InputParser.run(args);


        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }


        return "result";
    }
}
/*
   + suma wydatków posła/posłanki o określonym imieniu i nazwisku POLA layers = wydatki POLA 0-19
   + wysokości wydatków na 'drobne naprawy i remonty biura poselskiego' layers = wydatki POLE 13
   + średniej wartości sumy wydatków wszystkich posłów
   + posła/posłanki, który wykonał najwięcej podróży zagranicznych
   + posła/posłanki, który najdłużej przebywał za granicą
   + posła/posłanki, który odbył najdroższą podróż zagraniczną //1 podróz czy suma kwoty na podroze
   + listę wszystkich posłów, którzy odwiedzili Włochy
 */
/* ARGS :
    sumexpenses name
    smallexpenses name
    avgexpenses kadencja
    mosttravels kadencja
    longesttravels kadencja



 */